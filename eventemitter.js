const EventEmitter = require('events');
const myEmitter = new EventEmitter();

myEmitter.once('event', () => {
  console.log('B');
});

myEmitter.on('event', () => {
  console.log('A');
});

myEmitter.emit('event');
