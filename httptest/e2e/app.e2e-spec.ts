import { HttptestPage } from './app.po';

describe('httptest App', () => {
  let page: HttptestPage;

  beforeEach(() => {
    page = new HttptestPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
