import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class HttpRestServiceService {

  constructor(private http: Http) { }

  getGetRequest() {
      return this.http.get('https://shielded-caverns-1626.herokuapp.com')
                      .map((res) => res.json())
                      .catch((error) => Observable.throw(error.json().error || 'Server error'));

  }
  getPostRequest() {
      return this.http.post('https://shielded-caverns-1626.herokuapp.com', {some: 'data'})
                      .map((res) => res.json())
                      .catch((error) => Observable.throw(error.json().error || 'Server error'));

  }


}
