import { Component } from '@angular/core';
import { HttpRestServiceService } from './http-rest-service.service';

@Component({
  selector: 'app-root',
  providers: [HttpRestServiceService],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private httpRestServiceService:HttpRestServiceService) {
    httpRestServiceService.getGetRequest().subscribe(({message}) => {
      this.title = message;
    });
  }

  title = 'app';

}
