import { TestBed, inject } from '@angular/core/testing';

import { HttpRestServiceService } from './http-rest-service.service';

describe('HttpRestServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HttpRestServiceService]
    });
  });

  it('should be created', inject([HttpRestServiceService], (service: HttpRestServiceService) => {
    expect(service).toBeTruthy();
  }));
});
