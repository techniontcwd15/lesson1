import { Component } from '@angular/core';
import { ResizeNoticeService } from './resize-notice.service';

@Component({
  selector: 'app-root',
  providers: [ResizeNoticeService],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(private resizeNoticeService: ResizeNoticeService) {
    resizeNoticeService.noticeResizeWindow().subscribe((value) => this.title = JSON.stringify(value));
  }

}
