import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class ResizeNoticeService {

  constructor() { }

  noticeResizeWindow() {
    return Observable.fromEvent(window, 'resize').map((e:any) => e.timeStamp);
  }

}
