import { TestBed, inject } from '@angular/core/testing';

import { ResizeNoticeService } from './resize-notice.service';

describe('ResizeNoticeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ResizeNoticeService]
    });
  });

  it('should be created', inject([ResizeNoticeService], (service: ResizeNoticeService) => {
    expect(service).toBeTruthy();
  }));
});
