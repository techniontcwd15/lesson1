import { Component, OnInit } from '@angular/core';
import { ResizeNoticeService } from '../resize-notice.service';

@Component({
  selector: 'app-other',
  providers: [ResizeNoticeService],
  templateUrl: './other.component.html',
  styleUrls: ['./other.component.css']
})
export class OtherComponent implements OnInit {
  othervalue;

  constructor(private resizeNoticeService: ResizeNoticeService) {
    resizeNoticeService.noticeResizeWindow().subscribe((value) => this.othervalue = JSON.stringify(value));
  }

  ngOnInit() {
  }

}
