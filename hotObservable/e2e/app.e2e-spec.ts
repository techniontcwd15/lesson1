import { HotObservablePage } from './app.po';

describe('hot-observable App', () => {
  let page: HotObservablePage;

  beforeEach(() => {
    page = new HotObservablePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
