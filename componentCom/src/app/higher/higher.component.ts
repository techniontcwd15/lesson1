import { Component, Output, Input, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-higher',
  templateUrl: './higher.component.html',
  styleUrls: ['./higher.component.css']
})
export class HigherComponent implements OnInit {
  @Input() text = 'initial';
  @Output() emitter = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  fireEvent() {
    this.emitter.emit('wow');
  }
}
