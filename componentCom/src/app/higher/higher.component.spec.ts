import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HigherComponent } from './higher.component';

describe('HigherComponent', () => {
  let component: HigherComponent;
  let fixture: ComponentFixture<HigherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HigherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HigherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
